# -*- coding: iso-8859-1 -*-

'''
Created on 25/09/2013

@author: dmtic
'''
import sys
sys.path.append("C:\\Users\\Vagner\\Desktop\\Software Analysis Inmetro")

from control.dynamic.FlowExecution import FlowExecution
from control.hibrid.SuspiciousCode import SuspiciousCode

from time import localtime, strftime, gmtime
from idaapi import get_root_filename
from control.static.Disassembly import Disassembly
from control.static.Matrix import Matrix

print "\n\n\n============================== INICIO =============================="
print "Script: detector.py    " + "Programa: " + get_root_filename()
print "Data: " + strftime("%d-%m-%Y", gmtime()) + "    Hora: " + strftime("%H:%M:%S", localtime())
print "\n\n"


static = Disassembly()
static.execute()
static.collectFunctions()
static.refined()

matrix = Matrix()
matrix.binary = static.binary
matrix.adjacencyListBlocks()
matrix.adjacencyListFunctons()

flowExecution = FlowExecution()
flowExecution.binary = static.binary

suspiciousCode = SuspiciousCode()
suspiciousCode.binary = static.binary

noExecuted = {}
for i in range(0, 1):
		path = "C:\\Users\\dmtic\\Desktop\\result\\test\\test_1.exe"
		args = "20 10 87"
		sdir = "C:\\Users\\dmtic\\Desktop\\result\\test"
		
		dynamic = flowExecution.execute("", args, "", 500)
		
		instructions = static.binary.instructions
		for item in suspiciousCode.execute(instructions, dynamic).items():
				noExecuted.update({item[0] : item[1]})
		
suspiciousCode.printResult(noExecuted)
suspiciousCode.printResultRefined(noExecuted)
