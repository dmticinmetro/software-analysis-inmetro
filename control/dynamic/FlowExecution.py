#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

'''
Created on 25/09/2013

@author: dmtic
'''
from control.util.FileUtil import FileUtil
from idaapi import get_root_filename
from idc import LoadDebugger, StartDebugger, AddBpt, GetDebuggerEvent, GetDisasm, here, StepInto,\
    StepUntilRet, StopDebugger, WFNE_SUSP
from model.BinaryModel import BinaryModel

class FlowExecution():
    '''
    Coleta informa��es sobre a execu��o do programa.
    '''
    
    def __init__(self):
        self.binary = BinaryModel()
                    
    def __getattr__(self, name):
        return self.__dict__[str(name)]

    def __setattr__(self, name, value):
        self.__dict__[str(name)] = value



    def execute(self, path, args, sdir, quantInstr):
        '''
        Coleta o fluxo de execu��o do programa, por meio da depura��o.
    
        @param path: Caminho para o programa que ser� analisado.
        @param args: Argumentos do programa.
        @param sdir: Diret�rio inicial para o processo.
        @param quantInstr: Quantidade m�xima de instru��es a serem executadas
        
        @return: Lista das instru��es coletadas durante a depura��o.
        '''
    
        util = FileUtil()
        fileIO = util.fileIO(str(get_root_filename()).split('.')[0] + "_dinamico")
        util.sign(fileIO)
    
        # Lista das instru��es
        instructions = {}
                
        # Carrega o debugger (Par�metros: debugger module name Ex. "win32", "linux", "mac" / 0 = nao remoto, 1 = remoto})
        LoadDebugger("win32", 0)
        
        # Inicia o debugger
        StartDebugger("", args, "")
        
        # Executa at� o momento em que encontra o endere�o inicial da an�lise
        AddBpt(self.binary.firstInstruction)
        
        # Espera um evento e captura a execu��o
        GetDebuggerEvent(WFNE_SUSP, -1)
        
        # Captura a instru��o
        asm = GetDisasm(here()).split(';')[0].strip()
        
        instructions.update({here() : asm})
        fileIO.write("\n\n\n\n" + str(hex(here())) + ":    " + asm)
        
        # Vari�vel que controla a itera��o da execu��o passo a passo
        flagDebbuger = True
    
        # Contador de instru��es executadas
        countInstr = 1
        
        # Itera��o passo a passo da execu��o do programa
        while flagDebbuger and countInstr < quantInstr:
                
                # Executa uma instru��o do programa
                StepInto()
                
                # Verifica o resultado do GetDebuggerEvent ( > 0  = executada uma instru��o )
                if GetDebuggerEvent(WFNE_SUSP, -1) > 0:
                        
                        # Verifica se a instru��o pertence a �rea desejada
                        if here() > self.binary.start and here() < self.binary.end:
                                
                                # Captura a instru��o do cursor
                                asm = GetDisasm(here()).split(';')[0].strip()
                                
                                instructions.update({here() : asm})
                                fileIO.write("\n" + str(hex(here())) + ":    " + asm)
    
                                countInstr = countInstr + 1
                                
                        else:
                                
                                # Executa as instru��es sem depura��o at� o pr�ximo retorno
                                StepUntilRet()
                                
                else:
                        flagDebbuger = False
        
        # Para o debbuger
        StopDebugger()
        
        fileIO.write("\n\n\n\nRELAT�RIO AN�LISE DIN�MICA:\n")
        fileIO.write("\nQuantidade de Instru��es Executadas: " + str(len(instructions)))
        fileIO.close()
        
        return instructions