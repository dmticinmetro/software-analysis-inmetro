#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

'''
Created on 25/09/2013

@author: dmtic
'''
from control.util.FileUtil import FileUtil
from idaapi import get_root_filename
from model.BinaryModel import BinaryModel

class SuspiciousCode:
    '''
    Coleta informa��es sobre o arquivo bin�rio e a execu��o do programa.
    '''
    def __init__(self):
        self.binary = BinaryModel()
                    
    def __getattr__(self, name):
        return self.__dict__[str(name)]

    def __setattr__(self, name, value):
        self.__dict__[str(name)] = value
    
    
    
    def execute(self, disassembly, flowExecution):
            '''
            Realiza a an�lise do resultado da compara��o entre o disassembler e o fluxo de
            execu��o.
    
            @param disassembly: Lista com as instru��es do disassembly
            @param flowExecution: Lista com as instru��es do fluxo de execu��o
    
            @return: Lista com as instru��es encontradas com o disassembler e n�o executadas
                     durante a depura��o.
            '''
    
            # Lista das instru��es do disassembly
            instructions = disassembly
    
            # Itera��o sobre as instru��es da depura��o
            for fe in flowExecution.items():
                    
                    address = fe[0]
                    dynamic = fe[1]
                    
                    static = disassembly.get(address)
                    
                    # Compara as instru��es do disassembler e do fluxo de execu��o
                    if static[0] == dynamic:
                            
                            # Incrementa a contagem da instru��o
                            l = [static[0], static[1], static[2], static[3], static[4] + 1]
                            instructions.pop(address)
                            instructions.update({address : l})
            
            
            # Lista com as instru��es n�o executadas
            noExecuted = {}
            for instr in instructions.items():
                    if instr[1][4] == 0:
                            noExecuted.update({instr[0] : instr[1]})
                            
            return noExecuted
    
    
    
    def printResult(self, noExecuted):
            '''
            Imprime o resultado em um arquivo.
    
            @param noExecute: Lista com as instru��es encontradas com o disassembler e n�o executadas
                              durante a depura��o.
            '''
            util = FileUtil()
            fileIO = util.fileIO(str(get_root_filename()).split('.')[0] + "_final")
            util.sign(fileIO)
            
            if len(noExecuted) > 0:
                    
                    # Contador das instru��es analisadas n�o executadas
                    count = 0
    
                    # Vari�veis que armazenam a fun��o e o bloco que est�o em uso
                    function = ""
                    block = ""
    
                    # Recebe e ordena o dicion�rio pela chave
                    keylist = noExecuted.keys()
                    keylist.sort()
                        
                    for k in keylist:
                        for funcAnalysis in self.binary.functions:
                            addrStart = self.binary.functions.get(funcAnalysis)[0]
                            addrEnd = self.binary.functions.get(funcAnalysis)[1]
                            
                            if k in range(addrStart, addrEnd):
                                ne = noExecuted.get(k)
                                if function != ne[1]:
                                        function = ne[1]
                                        fileIO.write("\n\n\n\nFun��o: " + function)
                                        
                                if block != ne[2]:
                                        block = ne[2]
                                        fileIO.write("\n")
                                        
                                fileIO.write("\n" + str(hex(ne[3])) + ":    " + str(ne[0]))
                                count = count + 1
                                
                    fileIO.write("\n\n\n\nRELAT�RIO AN�LISE HIBR�DA:\n")
                    fileIO.write("\nResultado: " + str(count) + " instru��es n�o foram executadas.")
                    
            else:
                    fileIO.write("\n\n\n\nRELAT�RIO AN�LISE HIBR�DA:\n")
                    fileIO.write("\nResultado: Todas as instru��es foram executadas.")
                    
            fileIO.close()
            
            
            
    def printResultRefined(self, noExecuted):
        '''
        Imprime o resultado em um arquivo.

        @param noExecute: Lista com as instru��es encontradas com o disassembler e n�o executadas
                          durante a depura��o.
        '''
        util = FileUtil()
        fileIO = util.fileIO(str(get_root_filename()).split('.')[0] + "_final_refinado")
        util.sign(fileIO)
        
        if len(noExecuted) > 0:
                
                # Contador das instru��es analisadas n�o executadas
                count = 0

                # Vari�veis que armazenam a fun��o e o bloco que est�o em uso
                function = ""
                block = ""

                # Recebe e ordena o dicion�rio pela chave
                keylist = noExecuted.keys()
                keylist.sort()
                    
                for k in keylist:
                    for funcAnalysis in self.binary.functionsAnalysis:
                        addrStart = self.binary.functions.get(funcAnalysis)[0]
                        addrEnd = self.binary.functions.get(funcAnalysis)[1]
                        
                        if k in range(addrStart, addrEnd):
                            ne = noExecuted.get(k)
                            if function != ne[1]:
                                    function = ne[1]
                                    fileIO.write("\n\n\n\nFun��o: " + function)
                                    
                            if block != ne[2]:
                                    block = ne[2]
                                    fileIO.write("\n")
                                    
                            fileIO.write("\n" + str(hex(ne[3])) + ":    " + str(ne[0]))
                            count = count + 1
                            
                fileIO.write("\n\n\n\nRELAT�RIO AN�LISE HIBR�DA:\n")
                fileIO.write("\nResultado: " + str(count) + " instru��es n�o foram executadas.")
                
        else:
                fileIO.write("\n\n\n\nRELAT�RIO AN�LISE HIBR�DA:\n")
                fileIO.write("\nResultado: Todas as instru��es foram executadas.")
                
        fileIO.close()