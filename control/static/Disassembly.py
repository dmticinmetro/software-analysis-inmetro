#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

'''
Created on 25/09/2013

@author: dmtic
'''

from model.BinaryModel import BinaryModel
from control.util.FileUtil import FileUtil
from idaapi import get_root_filename, FlowChart, get_func
from idc import GetEntryPoint, GetFunctionAttr, FUNCATTR_START, FUNCATTR_END,\
    GetFunctionName, GetDisasm
from idc import GetEntryOrdinal, GetEntryPointQty, SegStart, SegEnd
from idautils import Functions, Heads
from control.static.Segments import Segments

class Disassembly():
    '''
    Coleta informa��es sobre o arquivo bin�rio do programa.
    '''
    def __init__(self):
        self.binary = BinaryModel()
        
    def __getattr__(self, name):
        return self.__dict__[str(name)]

    def __setattr__(self, name, value):
        self.__dict__[str(name)] = value
        
# ---------------------------------------------------------------------------------------------------- #
    def execute(self):
        '''
        Realiza o disassembler do arquivo bin�rio.
        
        @return: Lista das instru��es do arquivo bin�rio, com a seguinte estrutura:
                 [0] = instru��o, [1] = fun��o, [2] = bloco, [3] = endere�o, [4] = contagem.
        '''
        
        util = FileUtil()
        fileIO = util.fileIO(str(get_root_filename()).split('.')[0] + "_estatico")
        util.sign(fileIO)
        
        # Busca o endere�o inicial
        for ep in range(GetEntryPointQty()):
                self.binary.firstInstr = GetEntryPoint(GetEntryOrdinal(ep))
                
        # Seleciona um segmento
        seg = Segments().selectSegments([".text", "CODE"])[0]
    
        self.binary.start = SegStart(seg)
        self.binary.end = SegEnd(seg)
        self.binary.firstInstruction = self.binary.start
    
        # Vari�vel utilizada para criar um novo bloco
        flagBlock = False
    
        # Vari�veis para contar fun��es e blocos
        countFunctions = 0
        countBlocks = 0
        
        # Itera��o sobre as fun��es encontradas entre todos os endere�os
        for f in Functions(self.binary.start, self.binary.end):
                
                # Captura o endere�o inicial e final da fun��o
                funcStart = GetFunctionAttr(f, FUNCATTR_START)
                funcEnd = GetFunctionAttr(f, FUNCATTR_END)
                                
                funcName = GetFunctionName(f)
                self.binary.functions.update({funcName : [funcStart, funcEnd]})
                
                
                # Busca a fun��o inicial
                if funcName == "start" or funcName == "_main" or funcName == "sub_4016A0":
                        self.binary.firstInstruction = funcStart
                        
                fileIO.write("\n\n\n\nFun��o: " + funcName + " (Endere�o: " + hex(funcStart) + " - " + hex(funcEnd) + ")")
                        
                # Captura o CFG da fun��o
                cfg = FlowChart(get_func(f))
                
                # Vari�vel para contar a quantidade de blocos da fun��o
                count = 0
                
                # Itera��o sobre os blocos
                for b in cfg:
                        self.binary.blocks.update({funcName + "_" + str(count) : [[b.startEA, b.endEA], [], []]})
                        flagBlock = False
                        fileIO.write("\n")
                        
                        # Flag para verificar se deve ser incrementado a quantidade de blocos na pr�ximo itera��o.
                        # Obs: Isto foi colocado para atender a verifica��o da instru��o realizar no in�cio da pr�xima
                        # itera��o.
                        flagCount = True
                        
                        # Itera��o sobre as instru��es do bloco
                        for i in Heads(b.startEA, b.endEA):
                                
                                # Verifica se a instru��o est� dentro do espa�o do bloco
                                # Obs: Isto foi colocado devido a algumas incorretudes de estavam acontecendo, onde em 
                                # alguns blocos, uma instru��o do bloco seguinte era posta no bloco em quest�o.
                                if i in range(b.startEA, b.endEA):
                                    
                                    # Captura a instru��o
                                    asm = GetDisasm(i).split(';')[0].strip()
                                    
                                    fileIO.write("\n" + str(hex(i)) + ":    " + asm)
                                    
                                    # Adiciona instru��es na lista de instru��es
                                    self.binary.instructions.update({i : [asm, funcName, funcName + "_" + str(count), i, 0]})
                                    
                                    # Verifica se a instru��o � uma "call" para tratamento especial
                                    if asm.split(' ')[0] == "call":
                                            self.binary.instructionsCall.append(i)
                                            #flagBlock = True
                                            #fileIO.write("\n")
                                            #count = count + 1
                                            
                                    # Verifica se a instru��o � um "jump" para tratamento especial 
                                    if asm[0].lower() == "j":
                                            self.binary.instructionsJump.append(i)
                                            
                                else:
                                    flagCount = False
                                    
                        if flagCount:
                            count = count + 1
                                                
                if flagBlock:                        
                        fileIO.write("\nQuantidade de Blocos: " + str(count))
                        count = count - 1                       
                        
                else:
                        fileIO.write("\n\nQuantidade de Blocos: " + str(count))
                        
                countBlocks = countBlocks + count
                countFunctions = countFunctions + 1
                
        fileIO.write("\n\n\n\nRELAT�RIO AN�LISE EST�TICA:\n")
        fileIO.write("\nQuantidade de Fun��es: " + str(countFunctions))
        fileIO.write("\nQuantidade de Blocos B�sicos: " + str(countBlocks))
        fileIO.write("\nQuantidade de Instru��es: " + str(len(self.binary.instructions)))        
        fileIO.close()
        
        return self.binary
    
    
# ---------------------------------------------------------------------------------------------------- #
    def collectFunctions(self):
        '''
        Coleta informa��es sobre quais as fun��es que interessam ser analisadas.
        '''
        
        funcs = []
        
        funcs.append(self.binary.functions.get(self.binary.instructions.get(self.binary.firstInstruction)[1]))
        self.binary.functionsAnalysis.append(self.binary.instructions.get(self.binary.firstInstruction)[1])
        
        call = list(self.binary.instructionsCall)
        jump = list(self.binary.instructionsJump)
        
        while len(funcs) > 0:
            while len(funcs) > 0:
                f = funcs[0]
                if f != None:
                    for i in range(f[0], f[1]):
                        if i in call:
                            name = self.binary.instructions.get(i)[0].split("    ")[1]
                            if name in self.binary.functions.keys():
                                addr = self.binary.functions.get(name)
                                if name not in self.binary.functionsAnalysis:
                                    self.binary.functionsAnalysis.append(name)
                                    funcs.append(addr)
                                    call.remove(i)
                            else:
                                call.append(i)
                funcs.remove(f)
            
            for i in jump:
                    
                # Coleta a instru��o "jump" e a fun��o que ele pertence
                instr = self.binary.instructions.get(i)[0]
                func = self.binary.instructions.get(i)[1]
                
                # Verifica se a instur��o "jump" � de uma das fun��es a serema analisadas
                if func in self.binary.functionsAnalysis:
                        
                    # Verifica se � uma chamada para biblioteca din�mica
                    #if "ds:" not in instr:
                        
                    # Verifica se � uma chamada direta para uma fun��o
                    if "loc_" in instr:
                        loc = instr.split("loc_")[1]
                        name = self.binary.instructions.get(int(loc, 16))[1]
                        addr = self.binary.functions.get(name)
                        if name not in self.binary.functionsAnalysis:
                            self.binary.functionsAnalysis.append(name)
                            funcs.append(addr)
                    else:
                        jump.append(i)
                jump.remove(i)
                
        return self.binary
    
    
# ---------------------------------------------------------------------------------------------------- #    
    def refined(self):
        util = FileUtil()
        fileIO = util.fileIO(str(get_root_filename()).split('.')[0] + "_estatico_refinado")
        util.sign(fileIO)
        
        function = ""
        block = ""
        count = 0
        
        for f in self.binary.functionsAnalysis:
            start = self.binary.functions.get(f)[0]
            end = self.binary.functions.get(f)[1]
            
            for i in range(start, end):
                if self.binary.instructions.get(i) != None:
                    instr = self.binary.instructions.get(i)[0]
                    func = self.binary.instructions.get(i)[1]
                    bloc = self.binary.instructions.get(i)[2]
                    
                    if function != func:
                            function = func
                            fileIO.write("\n\n\n\nFun��o: " + function)
                            
                    if block != bloc:
                            block = bloc
                            fileIO.write("\n")
                            
                    fileIO.write("\n" + str(hex(i)) + ":    " + instr)
                    count = count + 1
        
        fileIO.write("\n\n\n\nRELAT�RIO AN�LISE EST�TICA:\n")
        fileIO.write("\nQuantidade de Fun��es: " + str(count))
        fileIO.close()
            
        return self.binary