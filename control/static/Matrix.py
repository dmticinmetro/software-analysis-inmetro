'''
Created on 06/10/2013

@author: Vagner
'''
from model.BinaryModel import BinaryModel
from control.util.FileUtil import FileUtil
from idaapi import get_root_filename

class Matrix():
    '''
    
    '''
    def __init__(self):
        self.binary = BinaryModel()
                    
    def __getattr__(self, name):
        return self.__dict__[str(name)]

    def __setattr__(self, name, value):
        self.__dict__[str(name)] = value
        
        
        
    def adjacencyListBlocks(self):
        util = FileUtil()
        fileIO = util.fileIO(str(get_root_filename()).split('.')[0] + "_adjacency_list_blocks")
        util.sign(fileIO)
        
        blocks = {}
        
        for b in self.binary.blocks.keys():
            blocks.update({b : [[], []]})
            inv = b[::-1]
            func = inv[inv.index("_") + 1 : len(inv)][::-1]
            num = b[::-1].split("_")[0]
            if int(num, 10) > 0:
                blockDest = func + "_" + str(int(num, 10) - 1)
                blocks.get(b)[0].append(blockDest)
                if blocks.get(blockDest) == None:
                    blocks.update({blockDest : [[], []]})
                blocks.get(blockDest)[1].append(b)
                
        for i in self.binary.instructionsCall:
            func = self.binary.instructions.get(i)[1]
            if func in self.binary.functionsAnalysis:
                blockOrig = self.binary.instructions.get(i)[2]
                blockDest = self.binary.instructions.get(i)[0].split('    ')[1] + "_0"            
                blocks.get(blockOrig)[1].append(blockDest)
                if blocks.get(blockDest) != None:
                    blocks.get(blockDest)[0].append(blockOrig)
            
        for i in self.binary.instructionsJump:
            func = self.binary.instructions.get(i)[1]
            if func in self.binary.functionsAnalysis:
                blockOrig = self.binary.instructions.get(i)[2]
                instr = self.binary.instructions.get(i)[0]
                if "loc_" in instr:
                    loc = instr.split("loc_")[1]
                    blockDest = self.binary.instructions.get(int(loc, 16))[2]
                    blocks.get(blockOrig)[1].append(blockDest)
                    blocks.get(blockDest)[0].append(blockOrig)
                        
                elif instr.split(" ")[len(instr.split(" ")) - 1] in self.binary.functions.keys():
                    blockDest = instr.split(" ")[len(instr.split(" ")) - 1] + "_0"
                    blocks.get(blockDest)[0].append(blockOrig)
                    blocks.get(blockOrig)[1].append(blockDest)
                        
        # ------------------------------ RESULTADO ------------------------------ #
        keylist = blocks.keys()
        keylist.sort()
        
        fileIO.write("\n\n\n\n");
        for k in keylist:
            v = blocks.get(k)
            inv = k[::-1]
            func = inv[inv.index("_") + 1 : len(inv)][::-1]
            
            if func in self.binary.functionsAnalysis:
                fileIO.write(k + ":    " + str(v[0]) +  "    " + str(v[1]) + "\n\n")
                print k + "  " + str(v) + "\n"
    
    
    
    def adjacencyListFunctons(self):
        util = FileUtil()
        fileIO = util.fileIO(str(get_root_filename()).split('.')[0] + "_adjacency_list_functions")
        util.sign(fileIO)
        
        funcs = {}
        
        for i in self.binary.instructionsCall:
            funcOrig = self.binary.instructions.get(i)[1]
            if funcOrig in self.binary.functionsAnalysis:
                funcDest = self.binary.instructions.get(i)[0].split('    ')[1]
                
                if funcs.get(funcOrig) == None:
                    funcs.update({funcOrig : [[], [funcDest]]})
                else:
                    funcs.get(funcOrig)[1].append(funcDest)
                    
                if funcs.get(funcDest) != None:
                    funcs.get(funcDest)[0].append(funcOrig)
                else:
                    funcs.update({funcDest : [[funcOrig], []]})
                                    
        # ------------------------------ RESULTADO ------------------------------ #
        keylist = funcs.keys()
        keylist.sort()
        
        fileIO.write("\n\n\n\n");
        for k in keylist:
            if k in self.binary.functionsAnalysis:
                v = funcs.get(k)
                fileIO.write(k + ":    " + str(v[0]) +  "    " + str(v[1]) + "\n\n")
                print k + "  " + str(v) + "\n"