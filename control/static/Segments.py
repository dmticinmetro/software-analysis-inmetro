# -*- coding: iso-8859-1 -*-

'''
Created on 25/09/2013

@author: dmtic
'''
from idc import FirstSeg, NextSeg, SegName

class Segments():
    def segments(self):
            '''
            Coleta os segmentos do arquivo bin�rio.
    
            @return Lista com os segmentos do bin�rio.
            '''
            segments = []
            
            # Recupera o endere�o do primeiro segmento
            seg = FirstSeg()
            segments.append(seg)
            
            # Recupera o endere�o do pr�ximo segmento
            seg = NextSeg(seg)
            
            while SegName(seg) != "":
                    segments.append(seg)
                    seg = NextSeg(seg)
    
            return segments
    
    def selectSegments(self, nameSeg):
            '''
            Seleciona os segmentos informados no par�metro "nameSeg".
    
            @param nameSeg: Lista com os nomes dos segmentos desejados
    
            @return Lista dos segmentos selecionados.
            '''
            segs = []
            for s in Segments().segments():
                    
                    # Define o segmento que sera utilizado
                    if SegName(s) in nameSeg:
                            segs.append(s)
                            
            return segs