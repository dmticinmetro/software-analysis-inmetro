# -*- coding: iso-8859-1 -*-

'''
Created on 25/09/2013

@author: dmtic
'''

from time import localtime, strftime, gmtime
from idaapi import get_root_filename

class FileUtil():
        '''
        M�todos utilizados em outras partes do c�digo para realizar opera��es de escrita e leitura em arquivos.
        '''
        def fileIO(self, name):
                '''
                Define e prepar� o arquivo para escrita.

                @param name: Nome do arquivo.

                @return: Arquivo para aberto para escrita.
                '''
                path = "C:\\Users\\Vagner\\Documents\\Meus Livros\\TI\\Inmetro\\An�lise de Software\\C�digo Suspeito\\Implementa��es\\result\\"
                #path = "C:\\Users\\Vagner\\Desktop\\result\\"
                #path = "C:\\Users\\dmtic\\Desktop\\result\\"
                fileIO = open(path + name + ".txt", "w")

                return fileIO
        
        def sign(self, fileIO):
                '''
                Realiza a assinatura do arquivo.
                
                @param file: Arquivo para escrita.
                '''
                fileIO.write("Script: detector.py")
                fileIO.write("\nPrograma: " + get_root_filename())
                fileIO.write("\nData: " + strftime("%d-%m-%Y", gmtime()) + "    Hora: " + strftime("%H:%M:%S", localtime()))