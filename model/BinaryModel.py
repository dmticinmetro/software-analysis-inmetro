# -*- coding: iso-8859-1 -*-

'''
Created on 25/09/2013

@author: dmtic
'''

class BinaryModel():
    '''
    Armazena informa��es do arquivo bin�rio.
    '''
    def __init__(self):
        
        # Endere�o inicial e final do segmento
        self.start = 0
        self.end = 0
        
        # Endere�o da primeira instru��o
        self.firstInstruction = 0
        
        # Lista com as instru��es, com uma estrutura {endere�o : [instru��o, fun��o, bloco, endere�o, contagem]}
        self.instructions = {}
        
        # Lista com as fun��es, com uma estrutura {nome da fun��o : [endere�o in�cial da fun��o, endere�o final da fun��o]}
        self.functions = {}
        
        # Lista com os blocos, com uma estrutura {id do bloco : [[endere�o in�cial da fun��o, endere�o final da fun��o], [blocos de entrada], [blocos de sa�da]]}
        self.blocks = {}
        
        # Lista de instru��es "call" e "jump"
        self.instructionsCall = []
        self.instructionsJump = []
        
        # Lista das instru��es que devem ser analisadas
        self.functionsAnalysis = []
                
    def __getattr__(self, name):
        return self.__dict__[str(name)]

    def __setattr__(self, name, value):
        self.__dict__[str(name)] = value