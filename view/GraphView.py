'''
Created on 30/09/2013

@author: dmtic
'''

from idaapi import GraphViewer

class GraphFinal():
    
    def execute(self):        
        graph = GraphViewer("Resultado Final")
        graph.Clear()
        graph.Refresh()
        
        obj1 = ["mov", "call", "call"]
        obj2 = ["push", "push", "add", "retn"]
        obj3 = ["exit"]
        
        id1 = graph.AddNode(obj1)
        id2 = graph.AddNode(obj2)
        id3 = graph.AddNode(obj3)
        graph.AddEdge(id1, id2)
        graph.AddEdge(id1, id3)
        
        graph.Show()